# Flow for search for favorite director
from utils.config import x_width, delimeter
from utils.common import getYearFilter, displayMoviesList, exit
from utils.database import mydb

def searchMoviesOfDirector():
   print("")
   print('Who is your favorite movie director?'.center(x_width,delimeter))
   director_name = input(" Enter director name: ")
   mycursor = mydb.cursor()
   year = getYearFilter()
   #rating = getRating()
   rating = input(" Enter the rating threshold above which you want to see the results (number between 1 to 10) : ")
   where_statement = "where imdb_title in "
   director_subquery = '(select imdb_title from directors where director_name = "'+ director_name +'") and '
   where_statement += director_subquery
   if year:
      where_statement += 'year(date_published) >= ' + str(year) + ' and '
   if rating:
      where_statement += 'avg_vote >= ' + rating + ' ' 
   sql = 'Select title, year(date_published), language, genre, avg_vote, director, writer, actors, description from movies '
   sql += where_statement + " order by avg_vote desc limit 20;"
   #print(sql)
   mycursor.execute(sql)
   moviesList = mycursor.fetchall()
   displayMoviesList(moviesList)
   exit()
# Flow for Search for movies of your favorite star
from utils.config import x_width, delimeter, movie_genres
from utils.common import getYearFilter, getRating, displayMoviesList, exit
from utils.database import mydb

def searchMoviesOfStar():
   print("")
   print('Who is your favorite movie star?'.center(x_width,delimeter))
   actor_name = input(" Enter movie star name: ")
   mycursor = mydb.cursor()
   year = getYearFilter()
   #rating = getRating()
   rating = input(" Enter the rating threshold above which you want to see the results (number between 1 to 10) : ")
   where_statement = "where imdb_title in "
   actor_subquery = '(select imdb_title from actors where actor_name = "'+actor_name+'") and '
   where_statement += actor_subquery
   if year:
      where_statement += 'year(date_published) >= ' + str(year) + ' and '
   if rating:
      where_statement += 'avg_vote >= ' + rating + ' ' 
   #select title from movies where imdb_title in (select imdb_title from actors where actor_name = "John Tait") and avg_vote > 6;
   sql = 'Select title, year(date_published), language, genre, avg_vote, director, writer, actors, description from movies '
   sql += where_statement + " order by avg_vote desc limit 20;"
   #print(sql)
   mycursor.execute(sql)
   moviesList = mycursor.fetchall()
   displayMoviesList(moviesList)
   exit()

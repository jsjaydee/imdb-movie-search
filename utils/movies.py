# Flow for search for favorite movie
from utils.config import x_width, delimeter, movie_genres
from utils.common import getYearFilter, getMovieGenre, getRating, displayMoviesList, exit
from utils.database import mydb

def searchMovie():
   mycursor = mydb.cursor()
   print('')
   #print(db_name)
   print('What kind of movie you would like to watch?'.center(x_width,delimeter))
   language = input(" Enter movie language: ")
   year = getYearFilter()
   genre = getMovieGenre()
   rating = getRating()
   where_statement = "where "
   if year:
      where_statement += 'year(date_published) >= ' + str(year) + ' and '
   if genre:
      where_statement += 'genre like "%' + genre + '%" and '
   if rating:
      where_statement += 'avg_vote >= ' + rating + ' and ' 
   if language:
      where_statement += 'language like "%' + language + '%"'
   sql = 'Select title, year(date_published), language, genre, avg_vote, director, writer, actors , description from movies '
   sql += where_statement + " order by avg_vote desc limit 20;"
   #print(sql)
   mycursor.execute(sql)
   moviesList = mycursor.fetchall()
   displayMoviesList(moviesList)
   exit()

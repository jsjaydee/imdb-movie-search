# Flow for Get Information about celebrity
from utils.config import x_width, delimeter
from utils.database import mydb
from utils.common import exit

def getCelebrityInfo():
   print('')
   print('You want to know details about which celebrity?'.center(x_width,delimeter))
   star_name = input(" Enter Celebrity name : ")
   mycursor = mydb.cursor()
   sql = 'Select date_of_birth, place_of_birth, height, bio from names where name = "' + star_name + '" limit 1;'
   #print(sql)
   mycursor.execute(sql)
   star_details = mycursor.fetchall()
   sql_2 = 'select title from movies where imdb_title in (select imdb_title from actors where actor_name = "'+star_name+'") order by avg_vote desc limit 10;'
   mycursor_2 = mydb.cursor()
   mycursor_2.execute(sql_2)
   #print(sql)
   #print(sql_2)
   star_movies = mycursor_2.fetchall()
   star_movies_list = [movie[0] for movie in star_movies]
   star_movies_string = ",".join(star_movies_list)
   print (("Details of "+ star_name).center(x_width,delimeter))
   for star in star_details:
      if star[0]:
         print(" Date of Birth: ", star[0])
      if star[1]:
         print(" Place of Birth: ", star[1])
      if star[2]:
         print(" Height: ", star[2], "cm")
      if star_movies_string:
         print(" Top Rated Movies: " ,star_movies_string)
      if star[3]:
         print(" Biography: \n", star[3])
      print("-".center(x_width,delimeter))
   print('SUCCESS'.center(x_width,delimeter))
   exit()

# functions common accross different flows
import datetime
import os
import sys
from utils.config import x_width, delimeter, movie_genres


def getYearFilter():
   print(" How old would you like the movie to be?")
   print(" 1. Doesn't matter")
   print(" 2. Released in last 3 years")
   print(" 3. Released in last 5 years")
   print(" 4. Released in last 10 years")
   year_input = int(input(" Enter your choice: "))
   current_date = datetime.datetime.now()
   year_filter = ""
   if year_input == 2:
      year_filter = current_date.year - 3
   if year_input == 3:
      year_filter = current_date.year - 5
   if year_input == 4:
      year_filter = current_date.year - 10
   return year_filter

def getMovieGenre():
   print(" Select Movie Genre:")
   for key in movie_genres:
      print(" " + key + ". " +movie_genres[key])
   input_val = input(" Enter your choice: ")
   if input_val in movie_genres:
      return movie_genres[input_val]
   else:
      print(" Please select a valid genre")
      getMovieGenre()

def getRating():
   print(" Do you want to view movies above a particular rating?")
   print(" 1. Yes")
   print(" 2. No")
   if (int(input(" Enter Your choice: ")) == 1):
      return input(" Enter the rating threshold above which you want to see the results (integer between 1 to 10) : ")
   else:
      return ""

def displayMoviesList(moviesList):
   if (len(moviesList) == 0):
      print(" Sorry, No results Found, But You can always add a movie to our records from main menu!!")
   else:
      print ("Search Results".center(x_width,delimeter))
      for movie in moviesList:
         print(" Movie name: ", movie[0])
         print(" Release year: ", movie[1])
         print(" Language: ", movie[2])
         print(" Genre: ", movie[3])
         print(" Rating: ", movie[4])
         print(" Director: ", movie[5])
         print(" Writer: ", movie[6])
         print(" Actors: ", movie[7])
         print(" Description: ", movie[8])
         print("-".center(x_width,delimeter))
      print('SUCCESS'.center(x_width,delimeter))

def exit():
    print(" Press 1 to exit")
    print(" Press 2 to return to main menu")
    n = int(input(" Enter your choice: "))
    if n == 1:
      sys.exit(0)
    if n == 2:
       os.system('python imdb-movies.py')
    else:
       print(" Invalid Option")
       exit()

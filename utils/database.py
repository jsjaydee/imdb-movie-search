import mysql.connector

db_host = "localhost"
db_user = "root"
db_passwd = "root"
db_name = "imdb_movies"

mydb = mysql.connector.connect(
 host = db_host,
 user = db_user,
 passwd = db_passwd
)

def initDB():
    mycursor = mydb.cursor()
    mycursor.execute('USE ' + db_name)
    #print(db_name)
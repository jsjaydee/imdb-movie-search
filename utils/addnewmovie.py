# Flow for search for movie based on age and gender
from utils.config import x_width, delimeter, movie_genres
from utils.common import getYearFilter, getMovieGenre, exit
from utils.database import mydb

def getIMdbTitle():
   mycursor = mydb.cursor()
   sql = "select substring(imdb_title,3,7) +1 from movies order by imdb_title DESC limit 1;"
   mycursor.execute(sql)
   title = mycursor.fetchall()
   new_title = "tt" + str(int(title[0][0]) + 1)
   return new_title

def addMovieToDatabase():
   mycursor = mydb.cursor()
   print('')
   print(' Add your favorite movie in the database.'.center(x_width,delimeter))
   print(' Please enter only correct information!!'.center(x_width,delimeter))
   imdb_title =  getIMdbTitle()
   title = input(' Enter movie name: ')
   date_published = input(' Enter release date (yyyy-mm-dd) : ')
   country = input(' Enter movie country : ')
   language = input(' Enter movie languages (comma seperated values) : ')
   genre = input(' Enter movie genres (comma seperated values) : ')
   duration = input(' Enter movie duration (runtime in mins) : ')
   actors = input(' Enter movie actors (comma seperated values) :')
   directors = input(' Enter movie directors (comma seperated values) :')  
   writers = input(' Enter movie witers (comma seperated values) :')
   production_company = input(' Enter production company :')
   values_to_be_inserted = '('+'"'+imdb_title+'","' + title +'","'+ title +'","' +date_published+'","'+country+'","' + language+'","' +genre+'","' + duration+'","' + actors+'","' + directors+'","' +writers+'","'+production_company+'")'
   sql = "insert into movies (imdb_title, title, original_title, date_published, country, language, genre, duration, actors, director, writer , production_company) values "+ values_to_be_inserted + ";"
   #print(sql)
   mycursor.execute(sql)
   mydb.commit()
   print( " Movie "+ title + " inserted into database successfully!!")
   exit()

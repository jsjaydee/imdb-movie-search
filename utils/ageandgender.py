# Flow for search for movie based on age and gender
from utils.config import x_width, delimeter, movie_genres
from utils.common import getYearFilter, getMovieGenre, exit
from utils.database import mydb

def getMoviesBasedOnAge():
   mycursor = mydb.cursor()
   print('')
   #print(db_name)
   print('Get popular movies for specific age and gender!!'.center(x_width,delimeter))
   print(' What is your Gender?')
   print(' 1. Male')
   print(' 2. Female')
   gender = int(input(" Enter your gender: "))
   print(' Which Age group you lie in?')
   print(' 1. Less than 18 yrs')
   print(' 2. Between 18 to 30 yrs')
   print(' 3. Between 30 to 45 yrs')
   print(' 4. 45yrs and above')
   age_group = int(input(" Enter your age group: "))
   query_column_name = ""
   query_table_name = ""
   if gender == 1:
      query_table_name = "age_specific_male_avg_ratings"
      query_column_name += "males_"
      user_gender = "males"
   if gender == 2:
      query_table_name = "age_specific_female_avg_ratings"
      query_column_name += "females_"
      user_gender = "females"
   if age_group == 1:
      query_column_name += "0age_"
   if age_group == 2:
      query_column_name += "18age_"
   if age_group == 3:
      query_column_name += "30age_"
   if age_group == 4:
      query_column_name += "45age_"
   query_column_name += "avg_vote"
   language = input(" Enter movie language: ")
   year = getYearFilter()
   genre = getMovieGenre()
   where_statement = "where "
   if year:
      where_statement += 'year(date_published) >= ' + str(year) + ' and '
   if genre:
      where_statement += 'genre like "%' + genre + '%" and '
   if language:
      where_statement += 'language like "%' + language + '%" and '
   where_statement += (query_column_name +'>= 8 ')
   order_by_statement = "order by "+ query_column_name + " desc limit 20;"
   sql = 'Select title, year(date_published), language, genre, avg_vote, director, writer, actors , description,' + query_column_name +' from movies inner join '+ query_table_name +' on imdb_title = imdb_title_id '+ where_statement + order_by_statement
   #print(sql)
   mycursor.execute(sql)
   moviesList = mycursor.fetchall()
   if (len(moviesList) == 0):
      print(" Sorry, No results Found, But You can always add a movie to our records from main menu!!")
   else:
      print ("Search Results".center(x_width,delimeter))
      for movie in moviesList:
         print(" Movie name: ", movie[0])
         print(" Release year: ", movie[1])
         print(" Language: ", movie[2])
         print(" Genre: ", movie[3])
         print(" Rating: ", movie[4])
         print(" Average Rating by " + user_gender + " of your age group: " , movie[9])
         print(" Director: ", movie[5])
         print(" Writer: ", movie[6])
         print(" Actors: ", movie[7])
         print(" Description: ", movie[8])
         print("-".center(x_width,delimeter))
      print('SUCCESS'.center(x_width,delimeter))
   exit()
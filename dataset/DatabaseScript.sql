DROP TABLE IF EXISTS movies;
CREATE TABLE movies
  (
     imdb_title            VARCHAR(255) PRIMARY KEY,
     title                 VARCHAR(255) NOT NULL,
     original_title        VARCHAR(255) NOT NULL,
     year                  INT,
     date_published        DATETIME NOT NULL,
     genre                 VARCHAR(255),
     duration              INT NOT NULL,
     country               VARCHAR(255),
     language              VARCHAR (255),
     director              VARCHAR(500),
     writer                VARCHAR(255),
     production_company    VARCHAR(255),
     actors                VARCHAR(500),
     description           VARCHAR(255),
     avg_vote              DECIMAL(10, 2),
     votes                 INT,
     budget                VARCHAR(255),
     usa_gross_income      VARCHAR(255),
     worlwide_gross_income VARCHAR(255),
     metascore             VARCHAR(255),
     reviews_from_users    INT,
     reviews_from_critics  INT
  )
engine=innodb
DEFAULT charset=utf8mb4
COLLATE=utf8mb4_general_ci;


load data infile 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/IMDb movies.csv' ignore into table movies CHARACTER SET utf8mb4 fields terminated by ',' enclosed by '"' lines terminated by '\n' ignore 1 rows
(imdb_title, title, original_title, year, date_published, genre, duration, country, @language, director, writer, production_company, actors, description, avg_vote, votes,
  @budget, @usa_gross_income, @worlwide_gross_income, @metascore, @reviews_from_users, @reviews_from_critics)
set language = if(@language = '', NULL, @language),
budget = if(@budget = '', NULL, @budget),
usa_gross_income = if(@usa_gross_income = '', NULL, @usa_gross_income),
worlwide_gross_income = if(@worlwide_gross_income = '', NULL, @worlwide_gross_income),
metascore = if(@metascore = '', NULL, @metascore),
reviews_from_users = if(@reviews_from_users = '', NULL, @reviews_from_users),
reviews_from_critics = if(@reviews_from_critics = '', NULL, @reviews_from_critics)
;
--     ignore 1 lines ;


DROP TABLE IF EXISTS genres;
CREATE TABLE genres
  (
     imdb_title VARCHAR(255),
     genre      VARCHAR(255),
     PRIMARY KEY(imdb_title, genre),
     FOREIGN KEY (imdb_title) REFERENCES movies(imdb_title)
  )
engine=innodb
DEFAULT charset=utf8mb4
COLLATE=utf8mb4_general_ci;


insert into genres select
  movies.imdb_title,
  LTRIM(SUBSTRING_INDEX(SUBSTRING_INDEX(movies.genre, ',', numbers.n), ',', -1)) genres
from
  (select 1 n union all
   select 2 union all select 3 union all
   select 4 union all select 5 ) numbers INNER JOIN movies
  on CHAR_LENGTH(movies.genre)
     -CHAR_LENGTH(REPLACE(movies.genre, ',', ''))>=numbers.n-1
order by
  imdb_title, n;


DROP TABLE IF EXISTS actors;
CREATE TABLE actors
  (
     imdb_title VARCHAR(255),
     actor_name VARCHAR(500),
     FOREIGN KEY (imdb_title) REFERENCES movies(imdb_title)
  )
engine=innodb
DEFAULT charset=utf8mb4
COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS directors;
CREATE TABLE directors
  (
     imdb_title    VARCHAR(255),
     director_name VARCHAR(500),
     FOREIGN KEY (imdb_title) REFERENCES movies(imdb_title)
  )
engine=innodb
DEFAULT charset=utf8mb4
COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS writers;
CREATE TABLE writers
  (
     imdb_title  VARCHAR(255),
     writer_name VARCHAR(255),
     FOREIGN KEY (imdb_title) REFERENCES movies(imdb_title)
  )
engine=innodb
DEFAULT charset=utf8mb4
COLLATE=utf8mb4_general_ci;


insert into actors select
  movies.imdb_title,
  LTRIM(SUBSTRING_INDEX(SUBSTRING_INDEX(movies.actors, ',', numbers.n), ',', -1)) actors
from
  (select 1 n union all
   select 2 union all select 3 union all
   select 4 union all select 5 union all
   select 6 union all select 7 union all
   select 8 union all select 9 union all
   select 10 union all select 11 union all
   select 12 union all select 13 union all
   select 14 union all select 15 union all
   select 16 union all select 17 union all
   select 18 union all select 19 union all
   select 20 union all select 21 union all
   select 22 union all select 23) numbers INNER JOIN movies
   on CHAR_LENGTH(movies.actors)-CHAR_LENGTH(REPLACE(movies.actors, ',', ''))>=numbers.n-1
   order by
   imdb_title, n;

    insert into directors select
      movies.imdb_title,
      LTRIM(SUBSTRING_INDEX(SUBSTRING_INDEX(movies.director, ',', numbers.n), ',', -1)) directors
    from
      (select 1 n union all
       select 2 union all select 3 union all
       select 4 union all select 5 ) numbers INNER JOIN movies
      on CHAR_LENGTH(movies.director)
         -CHAR_LENGTH(REPLACE(movies.director, ',', ''))>=numbers.n-1
    order by
      imdb_title, n;


      insert into writers select
        movies.imdb_title,
        LTRIM(SUBSTRING_INDEX(SUBSTRING_INDEX(movies.writer, ',', numbers.n), ',', -1)) writers
      from
        (select 1 n union all
         select 2 union all select 3 union all
         select 4 union all select 5 ) numbers INNER JOIN movies
        on CHAR_LENGTH(movies.writer)
           -CHAR_LENGTH(REPLACE(movies.writer, ',', ''))>=numbers.n-1
      order by
        imdb_title, n;


DROP TABLE IF EXISTS names;
CREATE TABLE names
  (
     imdb_name_id          VARCHAR(255) PRIMARY KEY,
     name                  VARCHAR(255) NOT NULL,
     birth_name            VARCHAR(255) NOT NULL,
     height                INT,
     bio                   TEXT,
     birth_details         VARCHAR(255),
     date_of_birth         DATETIME,
     place_of_birth        VARCHAR(255),
     spouses_string        VARCHAR(255),
     spouses               INT,
     divorces              INT,
     spouses_with_children INT,
     children              INT
  )
  engine=innodb
  DEFAULT charset=utf8mb4
  COLLATE=utf8mb4_general_ci;

load data infile 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/IMDb names.csv' ignore into table names CHARACTER SET utf8mb4 fields terminated by ',' enclosed by '"' lines terminated by '\n' ignore 1 rows
(imdb_name_id, name, birth_name, height, bio, @birth_details, @date_of_birth, @place_of_birth, @death_details, @date_of_death, @place_of_death, @reason_of_death, spouses_string, spouses, divorces, spouses_with_children, children)
set birth_details = if(@birth_details = '', NULL, @birth_details),
    date_of_birth = if(@date_of_birth = '', NULL, @date_of_birth),
    place_of_birth = if(@place_of_birth = '', NULL, @place_of_birth);



DROP TABLE IF EXISTS death_info;
CREATE TABLE death_info
  (
    imdb_name_id          VARCHAR(255) PRIMARY KEY,
    death_details         VARCHAR(255),
    date_of_death         DATETIME,
    place_of_death        VARCHAR(255),
    reason_of_death       VARCHAR(255),
    foreign key (imdb_name_id) references names(imdb_name_id)
  )
  engine=innodb
  DEFAULT charset=utf8mb4
  COLLATE=utf8mb4_general_ci;


  load data infile 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/IMDb names.csv' ignore into table death_info CHARACTER SET utf8mb4 fields terminated by ',' enclosed by '"' lines terminated by '\n' ignore 1 rows
  (imdb_name_id, @name, @birth_name, @height, @bio, @birth_details, @date_of_birth, @place_of_birth, @death_details, @date_of_death, @place_of_death, @reason_of_death, @spouses_string, @spouses, @divorces, @spouses_with_children, @children)
  set death_details = if(@death_details = '', NULL, @death_details),
      date_of_death = if(@date_of_death = '', NULL, @date_of_death),
      place_of_death = if(@place_of_death = '', NULL, @place_of_death),
      reason_of_death = if(@reason_of_death = '', NULL, @reason_of_death);


drop table if exists average_ratings;
CREATE TABLE average_ratings
  (
     imdb_title_id             VARCHAR(255) primary key,
     weighted_average_vote     DECIMAL(2, 1) not null,
     total_votes               INT not null,
     mean_vote                 DECIMAL(2, 1) not null,
     median_vote               DECIMAL(2, 1) not null,
     foreign key(imdb_title_id) references movies(imdb_title)
  )ENGINE=InnoDB DEFAULT charset=utf8mb4 COLLATE=utf8mb4_general_ci;

load data infile 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/IMDb ratings.csv' ignore into table average_ratings CHARACTER SET utf8mb4 fields terminated by ',' enclosed by '"' lines terminated by '\n' ignore 1 rows
(imdb_title_id, weighted_average_vote,	total_votes,	mean_vote,	median_vote);


drop table if exists age_specific_allgender_avg_ratings;
CREATE TABLE age_specific_allgender_avg_ratings
  (
     imdb_title_id             VARCHAR(255) primary key,
     allgenders_0age_avg_vote  DECIMAL(2, 1),
     allgenders_0age_votes     DECIMAL(2, 1),
     allgenders_18age_avg_vote DECIMAL(2, 1),
     allgenders_18age_votes    INT,
     allgenders_30age_avg_vote DECIMAL(2, 1),
     allgenders_30age_votes    INT,
     allgenders_45age_avg_vote DECIMAL(2, 1),
     allgenders_45age_votes    INT,
     foreign key(imdb_title_id) references movies(imdb_title)
  )ENGINE=InnoDB DEFAULT charset=utf8mb4 COLLATE=utf8mb4_general_ci;

  load data infile 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/IMDb ratings.csv' ignore into table age_specific_allgender_avg_ratings CHARACTER SET utf8mb4 fields terminated by ',' enclosed by '"' lines terminated by '\n' ignore 1 rows
  (imdb_title_id, @weighted_average_vote,	@total_votes,	@mean_vote,	@median_vote,	@votes_10,	@votes_9,	@votes_8,	@votes_7,	@votes_6,	@votes_5,	@votes_4,	@votes_3,	@votes_2,@votes_1,	@allgenders_0age_avg_vote,	@allgenders_0age_votes,	@allgenders_18age_avg_vote,	@allgenders_18age_votes,	@allgenders_30age_avg_vote,	@allgenders_30age_votes,	@allgenders_45age_avg_vote,	@allgenders_45age_votes,	@males_allages_avg_vote,	@males_allages_votes,	@males_0age_avg_vote,	@males_0age_votes,	@males_18age_avg_vote,	@males_18age_votes,	@males_30age_avg_vote,	@males_30age_votes,	@males_45age_avg_vote,	@males_45age_votes, @females_allages_avg_vote, @females_allages_votes,	@females_0age_avg_vote,	@females_0age_votes,	@females_18age_avg_vote,	@females_18age_votes,	@females_30age_avg_vote,	@females_30age_votes,	@females_45age_avg_vote,	@females_45age_votes,	@top1000_voters_rating,	@top1000_voters_votes,	@us_voters_rating,	@us_voters_votes, @non_us_voters_rating,	@non_us_voters_votes)
  set allgenders_0age_avg_vote = if(@allgenders_0age_avg_vote = '', NULL, @allgenders_0age_avg_vote),
      allgenders_0age_votes = if(@allgenders_0age_votes = '', NULL, @allgenders_0age_votes),
      allgenders_18age_avg_vote = if(@allgenders_18age_avg_vote = '', NULL, @allgenders_18age_avg_vote),
      allgenders_18age_votes = if(@allgenders_18age_votes = '', NULL, @allgenders_18age_votes),
      allgenders_30age_avg_vote = if(@allgenders_30age_avg_vote = '', NULL, @allgenders_30age_avg_vote),
      allgenders_30age_votes = if(@allgenders_30age_votes = '', NULL, @allgenders_30age_votes),
      allgenders_45age_avg_vote = if(@allgenders_45age_avg_vote = '', NULL, @allgenders_45age_avg_vote),
      allgenders_45age_votes = if(@allgenders_45age_votes = '', NULL, @allgenders_45age_votes);


  drop table if exists age_specific_male_avg_ratings;
  CREATE TABLE age_specific_male_avg_ratings
    (
       imdb_title_id             VARCHAR(255) primary key,
       males_allages_avg_vote    DECIMAL(2, 1),
       males_allages_votes       INT,
       males_0age_avg_vote       DECIMAL(2, 1),
       males_0age_votes          INT,
       males_18age_avg_vote      DECIMAL(2, 1),
       males_18age_votes         INT,
       males_30age_avg_vote      DECIMAL(2, 1),
       males_30age_votes         INT,
       males_45age_avg_vote      DECIMAL(2, 1),
       males_45age_votes         INT,
       foreign key(imdb_title_id) references movies(imdb_title)
    )ENGINE=InnoDB DEFAULT charset=utf8mb4 COLLATE=utf8mb4_general_ci;


  load data infile 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/IMDb ratings.csv' ignore into table age_specific_male_avg_ratings CHARACTER SET utf8mb4 fields terminated by ',' enclosed by '"' lines terminated by '\n' ignore 1 rows
  (imdb_title_id, @weighted_average_vote,	@total_votes,	@mean_vote,	@median_vote,	@votes_10,	@votes_9,	@votes_8,	@votes_7,	@votes_6,	@votes_5,	@votes_4,	@votes_3,	@votes_2,@votes_1,	@allgenders_0age_avg_vote,	@allgenders_0age_votes,	@allgenders_18age_avg_vote,	@allgenders_18age_votes,	@allgenders_30age_avg_vote,	@allgenders_30age_votes,	@allgenders_45age_avg_vote,	@allgenders_45age_votes,	@males_allages_avg_vote,	@males_allages_votes,	@males_0age_avg_vote,	@males_0age_votes,	@males_18age_avg_vote,	@males_18age_votes,	@males_30age_avg_vote,	@males_30age_votes,	@males_45age_avg_vote,	@males_45age_votes, @females_allages_avg_vote, @females_allages_votes,	@females_0age_avg_vote,	@females_0age_votes,	@females_18age_avg_vote,	@females_18age_votes,	@females_30age_avg_vote,	@females_30age_votes,	@females_45age_avg_vote,	@females_45age_votes,	@top1000_voters_rating,	@top1000_voters_votes,	@us_voters_rating,	@us_voters_votes, @non_us_voters_rating,	@non_us_voters_votes)
    set males_allages_avg_vote = if(@males_allages_avg_vote = '', NULL, @males_allages_avg_vote),
        males_allages_votes = if(@males_allages_votes = '', NULL, @males_allages_votes),
        males_0age_avg_vote = if(@males_0age_avg_vote = '', NULL, @males_0age_avg_vote),
        males_0age_votes = if(@males_0age_votes = '', NULL, @males_0age_votes),
        males_18age_avg_vote = if(@males_18age_avg_vote = '', NULL, @males_18age_avg_vote),
        males_18age_votes = if(@males_18age_votes = '', NULL, @males_18age_votes),
        males_30age_avg_vote = if(@males_30age_avg_vote = '', NULL, @males_30age_avg_vote),
        males_30age_votes = if(@males_30age_votes = '', NULL, @males_30age_votes),
        males_45age_avg_vote = if(@males_45age_avg_vote = '', NULL, @males_45age_avg_vote),
        males_45age_votes = if(@males_45age_votes = '', NULL, @males_45age_votes);


  drop table if exists age_specific_female_avg_ratings;
  CREATE TABLE age_specific_female_avg_ratings
    (
       imdb_title_id             VARCHAR(255) primary key,
       females_allages_avg_vote  DECIMAL(2, 1),
       females_allages_votes     INT,
       females_0age_avg_vote     DECIMAL(2, 1),
       females_0age_votes        INT,
       females_18age_avg_vote    DECIMAL(2, 1),
       females_18age_votes       INT,
       females_30age_avg_vote    DECIMAL(2, 1),
       females_30age_votes       INT,
       females_45age_avg_vote    DECIMAL(2, 1),
       females_45age_votes       INT,
       foreign key(imdb_title_id) references movies(imdb_title)
    )ENGINE=InnoDB DEFAULT charset=utf8mb4 COLLATE=utf8mb4_general_ci;


  load data infile 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/IMDb ratings.csv' ignore into table age_specific_female_avg_ratings CHARACTER SET utf8mb4 fields terminated by ',' enclosed by '"' lines terminated by '\n' ignore 1 rows
  (imdb_title_id, @weighted_average_vote,	@total_votes,	@mean_vote,	@median_vote,	@votes_10,	@votes_9,	@votes_8,	@votes_7,	@votes_6,	@votes_5,	@votes_4,	@votes_3,	@votes_2,@votes_1,	@allgenders_0age_avg_vote,	@allgenders_0age_votes,	@allgenders_18age_avg_vote,	@allgenders_18age_votes,	@allgenders_30age_avg_vote,	@allgenders_30age_votes,	@allgenders_45age_avg_vote,	@allgenders_45age_votes,	@males_allages_avg_vote,	@males_allages_votes,	@males_0age_avg_vote,	@males_0age_votes,	@males_18age_avg_vote,	@males_18age_votes,	@males_30age_avg_vote,	@males_30age_votes,	@males_45age_avg_vote,	@males_45age_votes, @females_allages_avg_vote, @females_allages_votes,	@females_0age_avg_vote,	@females_0age_votes,	@females_18age_avg_vote,	@females_18age_votes,	@females_30age_avg_vote,	@females_30age_votes,	@females_45age_avg_vote,	@females_45age_votes,	@top1000_voters_rating,	@top1000_voters_votes,	@us_voters_rating,	@us_voters_votes, @non_us_voters_rating,	@non_us_voters_votes)
    set females_allages_avg_vote = if(@females_allages_avg_vote = '', NULL, @females_allages_avg_vote),
        females_allages_votes = if(@females_allages_votes = '', NULL, @females_allages_votes),
        females_0age_avg_vote = if(@females_0age_avg_vote = '', NULL, @females_0age_avg_vote),
        females_0age_votes = if(@females_0age_votes = '', NULL, @females_0age_votes),
        females_18age_avg_vote = if(@females_18age_avg_vote = '', NULL, @females_18age_avg_vote),
        females_18age_votes = if(@females_18age_votes = '', NULL, @females_18age_votes),
        females_30age_avg_vote = if(@females_30age_avg_vote = '', NULL, @females_30age_avg_vote),
        females_30age_votes = if(@females_30age_votes = '', NULL, @females_30age_votes),
        females_45age_avg_vote = if(@females_45age_avg_vote = '', NULL, @females_45age_avg_vote),
        females_45age_votes = if(@females_45age_votes = '', NULL, @females_45age_votes);


  DROP TABLE IF EXISTS title_principal;
  CREATE TABLE title_principal
    (
       imdb_title_id VARCHAR(255),
       ordering      INT,
       imdb_name_id  VARCHAR(255),
       category      VARCHAR(255) NOT NULL,
       job           VARCHAR(255),
       PRIMARY KEY(imdb_title_id, imdb_name_id)
    )
  engine=innodb
  DEFAULT charset=utf8mb4
  COLLATE=utf8mb4_general_ci;



SET FOREIGN_KEY_CHECKS=0;



load data infile 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/IMDb title_principals.csv' ignore into table title_principal CHARACTER SET utf8mb4 fields terminated by ',' lines terminated by '\n' ignore 1 rows
(imdb_title_id,	ordering,	imdb_name_id,	category,	@job)
  set job = if(@job = '', NULL, @job);

alter table title_principal add constraint fk1 foreign key (imdb_title_id) references movies(imdb_title);
alter table title_principal add constraint fk2 foreign key (imdb_name_id) references names(imdb_name_id);
SET FOREIGN_KEY_CHECKS=1;

alter table names drop column birth_details;
alter table death_info drop column death_details;
alter table names drop column spouses_string;
alter table names drop column spouses_with_children;

alter table movies drop column year;
-- alter table movies drop column actors;
-- alter table movies drop column director;
-- alter table movies drop column writer;
-- alter table movies drop column genre;

create index idx_date_published on movies(date_published);
create index idx_language on movies(language);
create index idx_avg_vote on movies(avg_vote);
create index idx_imdb_title on actors(imdb_title);
create index idx_imdb_title1 on directors(imdb_title);
create index idx_imdb_title2 on writers(imdb_title);
create index idx_imdb_title3 on genres(imdb_title);
create index idx_actor_name on actors(actor_name);
create index idx_director_name on directors(director_name);
create index idx_writer_name on writers(writer_name);
create index idx_genre on genres(genre);
create index idx_name on names(name);
create index idx_allgenders_0age_avg_vote on age_specific_allgender_avg_ratings(allgenders_0age_avg_vote);
create index idx_allgenders_18age_avg_vote on age_specific_allgender_avg_ratings(allgenders_18age_avg_vote);
create index idx_allgenders_30age_avg_vote on age_specific_allgender_avg_ratings(allgenders_30age_avg_vote);
create index idx_allgenders_45age_avg_vote on age_specific_allgender_avg_ratings(allgenders_45age_avg_vote);
create index idx_males_0age_avg_vote on age_specific_male_avg_ratings(males_0age_avg_vote);
create index idx_males_18age_avg_vote on age_specific_male_avg_ratings(males_18age_avg_vote);
create index idx_males_30age_avg_vote on age_specific_male_avg_ratings(males_30age_avg_vote);
create index idx_males_45age_avg_vote on age_specific_male_avg_ratings(males_45age_avg_vote);
create index idx_females_0age_avg_vote on age_specific_female_avg_ratings(females_0age_avg_vote);
create index idx_females_18age_avg_vote on age_specific_female_avg_ratings(females_18age_avg_vote);
create index idx_females_30age_avg_vote on age_specific_female_avg_ratings(females_30age_avg_vote);
create index idx_females_45age_avg_vote on age_specific_female_avg_ratings(females_45age_avg_vote);


  -- DELIMITER //
  -- CREATE PROCEDURE insertvalues()
  --     BEGIN
  --       DECLARE startingRange INT DEFAULT 1;
  --        WHILE startingRange <= 50 DO
  --           INSERT numbers(n) VALUES (startingRange );
  --           SET startingRange = startingRange + 1;
  --        END WHILE;
  --     END
  --     //
  --     DELIMITER ;
  --
  --   call insertvalues();
  --
  --   create table actors(imdb_title varchar(255), actor_name varchar(500));
  --
  --     select
  --     movies.imdb_title,
  --     SUBSTRING_INDEX(SUBSTRING_INDEX(movies.actors, ',', numbers.n), ',', -1) actors
  --     from
  --       numbers inner join movies
  --       on CHAR_LENGTH(movies.actors)
  --          -CHAR_LENGTH(REPLACE(movies.actors, ',', ''))>=numbers.n-1
  --     order by
  --       imdb_title, n
  --       limit 5;
  -- load data infile 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/one.csv' ignore into table a2 fields terminated by ',' enclosed by '"' lines terminated by '\n' ignore 1 rows;


  -- select
  --   movies.imdb_title,
  --   LTRIM(SUBSTRING_INDEX(SUBSTRING_INDEX(movies.actors, ',', numbers.n), ',', -1)) actors
  -- from
  --   (select 1 n union all
  --    select 2 union all select 3 ) numbers INNER JOIN movies
  --   on CHAR_LENGTH(movies.actors)
  --      -CHAR_LENGTH(REPLACE(movies.actors, ',', ''))>=numbers.n-1
  -- order by
  --   imdb_title, n limit 5;

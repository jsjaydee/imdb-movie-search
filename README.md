# Imdb Movies Dataset
## The project exposes a command line interface for the user to search for favorite movie, actor, director or get the personal details of a celebrity. It also recommends movie based on the gender and age group of the user and also allows the user to add a movie to the database.
## Project usage:
### DB setup : 
#### Run the DBscript on mysql command line: source dataset/DatabaseScript.sql;  
### Client application:
#### Run the command: python imdb-movies.py
### DB configuration:
#### Change the MYSQL configuration (host, username, password and db_name) in utils/database.py to make sure it points to the right instance.

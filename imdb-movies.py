import os
import sys

from utils.common import x_width, delimeter
from utils.config import x_width, delimeter, movie_genres
from utils.common import getYearFilter, getMovieGenre, getRating, displayMoviesList
from utils.movies import searchMovie
from utils.moviestar import searchMoviesOfStar
from utils.director import searchMoviesOfDirector
from utils.celebrityinfo import getCelebrityInfo
from utils.ageandgender import getMoviesBasedOnAge
from utils.addnewmovie import addMovieToDatabase
from utils.database import mydb, initDB

def displayMainMenu():
    os.system('cls' if os.name == 'nt' else 'clear')
    print("")
    print('MAIN MENU'.center(x_width,delimeter))
    print(' 1. Get movie recommendation')
    print(' 2. Search for movies of your favorite star')
    print(' 3. Search for movies of your favorite director')
    print(' 4. Get information about your favorite celebrity')
    print(' 5. Get movie recommendation based on your Age/Gender')
    print(' 6. Add a movie to the database')
    print(' 7. Exit')
    print(delimeter.center(x_width, delimeter))
    return int(input(" Enter option : "))

def run():
   try:
      n = displayMainMenu()
      if n == 1:
         os.system('cls' if os.name == 'nt' else 'clear')
         searchMovie()
      if n == 2:
         os.system('cls' if os.name == 'nt' else 'clear')
         searchMoviesOfStar()
      if n == 3:
         os.system('cls' if os.name == 'nt' else 'clear')
         searchMoviesOfDirector()
      if n == 4:
         os.system('cls' if os.name == 'nt' else 'clear')
         getCelebrityInfo()
      if n == 5:
         os.system('cls' if os.name == 'nt' else 'clear')
         getMoviesBasedOnAge()
      if n == 6:
         os.system('cls' if os.name == 'nt' else 'clear')
         addMovieToDatabase()
      elif n == 7:
         print('Thank You'.center(x_width,delimeter))
         sys.exit(0)
      else:
         run()
   except KeyboardInterrupt:
      sys.exit(0)

if __name__ == '__main__':
   initDB()
   run()